//Librería usadas para el sensor BH1750 
#include <Wire.h>
#include <BH1750.h>

//Librería para hacer interrupciones por timer
#include <TimerOne.h>
//Librería propiedad de Andra para el anemometro
//Esta creada para el comparador LM393 
#include <Anemometer.h>
//Instanciamos un objetode la clase Anemometer
Anemometer Anemometro;

//Área del foto transistor
 //0.25 mm x 0.3 mm o 0.00025 m  x 0.0003 m 
 //área = 0.075 mm2 0.000000075 m2
double areaFoto = 0.000000075;

//eficiencia del sol (Lumens/ watts)
double  efiSol = 683.0;

//Instanciamos un objeto de la clase BH1750
BH1750 luxometro;

//Distintas resoluciones a alegir
const byte luxMode = BH1750_CONTINUOUS_HIGH_RES_MODE;
// BH1750_CONTINUOUS_HIGH_RES_MODE
// BH1750_CONTINUOUS_HIGH_RES_MODE_2
// BH1750_CONTINUOUS_LOW_RES_MODE
// BH1750_ONE_TIME_HIGH_RES_MODE
// BH1750_ONE_TIME_HIGH_RES_MODE_2
// BH1750_ONE_TIME_LOW_RES_MODE

void setup() {
  Serial.begin(9600);
  //BH1750
  Serial.println(F("Inicializando sensor..."));
  luxometro.begin(luxMode); // Inicializar BH1750
  //Anemometer Lm393
  Timer1.initialize(1000000); // establece el timer para interrupciones cada segundo o un millon de microsegundos
  attachInterrupt(1, docount, RISING);  // increase counter when speed sensor pin goes High
  Timer1.attachInterrupt(velocidad); // enable the timer

}

void loop() {
  uint16_t lux = luxometro.readLightLevel(); // Lectura del BH1750
  //Serial.print(F("Iluminancia:  "));
  Serial.print(lux);
  Serial.println("X"); 

  //conversión a lumens
  //Luxes = Lumenes/m2
  //Lumenes = Luxes * m2
  
  int luxesInt = (int)lux;
  double luxesDouble = (double)luxesInt;
  double lumenesDouble = luxesDouble * areaFoto;
  
  //Serial.print("Lumenes: ");
  //Serial.println(lumenesDouble,10);
  //calculo de los watts
  //Watts = lumenes / eficiencia de la fuente de luz (lumenes/watts)
  double watt = lumenesDouble /efiSol;

  //Serial.print("Watts:  ");
  Serial.print(watt,10);
  Serial.println("W");
  
  delay(1000);

}

void docount()  
{
  Anemometro.setCounter();
} 

void velocidad()
{
  Timer1.detachInterrupt();  //detiene el timer
  Anemometro.setVelAngular();
  Anemometro.setVelTangencialMS();
  Anemometro.setVelTangencialKMH();
  Anemometro.printVelAngular();
  Anemometro.printVelTangMS();
  Anemometro.printVelTangKMH();
  //Anemometro.retToZeroCounter();
  Timer1.attachInterrupt(velocidad);  //habilita el timer
  
}
